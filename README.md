ShockWave Server
================

Hi! Use this to run the ShockWave server.

Running the server
------------------

Currently only on Windows.

To install and run the server, you'll need to have the following tools in your `PATH`:

- [jq](https://stedolan.github.io/jq/)
- [node](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/)

> Bash, curl and other common Unix tools are shipped in Cygwin or Git For Windows.

Then simply* run `sh ./install.sh` and follow the instructions.

<small>* _Admin privileges or developer mode may be required to create necessary symlinks._</small>
