resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

file 'config.ini'

client_scripts {
	'nativeui.net.dll',
	'vstancer_shared.net.dll',
	'vstancer_client.net.dll'
}

server_scripts {
	'vstancer_shared.net.dll',
	'vstancer_server.net.dll'
}