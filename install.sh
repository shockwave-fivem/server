#!/bin/sh

source "./scripts/common.sh"
source "./scripts/defaults.sh"
source "./scripts/helpers.sh"

# ------------------------------------------------------------------------------

echo "-----------------------------"
echo "     SHOCKWAVE INSTALLER     "
echo "-----------------------------"

# ------------------------------------------------------------------------------

echo "Fetching submodules..."

git submodule init
git submodule update

# ------------------------------------------------------------------------------

echo "ShockWave will install now."

# Ensure clean temp dir
[[ -d $TMP_DIR ]] && rm -r $TMP_DIR
mkdir -p $TMP_DIR

# ------------------------------------------------------------------------------

if [ ! -d "./config" ]; then
  SW_HOSTNAME=$(read_input "Hostname" "$DEFAULT_SW_HOSTNAME")
  SW_RCON_PASSWORD=$(read_input "RCON Password" "$DEFAULT_SW_RCON_PASSWORD")
  SW_LICENSE_KEY=$(read_input "License key" "$DEFAULT_SW_LICENSE_KEY")
  SW_TEBEX_SECRET=$(read_input "Tebex secret" "$DEFAULT_SW_TEBEX_SECRET")
  SW_STEAM_API_KEY=$(read_input "Steam API key" "$DEFAULT_SW_STEAM_API_KEY")
  SW_ENDPOINT_TCP=$(read_input "TCP endpoint" "$DEFAULT_SW_ENDPOINT_TCP")
  SW_ENDPOINT_UDP=$(read_input "UDP endpoint" "$DEFAULT_SW_ENDPOINT_UDP")
  SW_MAX_CLIENTS=$(read_input "Max players" "$DEFAULT_SW_MAX_CLIENTS")

  export             \
    SW_HOSTNAME      \
    SW_RCON_PASSWORD \
    SW_LICENSE_KEY   \
    SW_TEBEX_SECRET  \
    SW_STEAM_API_KEY \
    SW_ENDPOINT_TCP  \
    SW_ENDPOINT_UDP  \
    SW_MAX_CLIENTS

  mkdir ./config
  cat ./scripts/data/_server.cfg | envsubst > ./config/_server.cfg

  cp ./scripts/data/default-server-permissions.cfg ./config/server-permissions.cfg
  cp ./scripts/data/default-vmenu-permissions.cfg ./config/vmenu-permissions.cfg
  cp ./scripts/data/default-autostart-resources.cfg ./config/autostart-resources.cfg

  echo "" >> ./config/_server.cfg
  echo "exec ./config/server-permissions.cfg" >> ./config/_server.cfg
  echo "exec ./config/vmenu-permissions.cfg" >> ./config/_server.cfg
  echo "exec ./config/autostart-resources.cfg" >> ./config/_server.cfg
fi

# ------------------------------------------------------------------------------

if [ ! -d "./.server" ]; then
  echo ""
  echo "Downloading FXServer..."

  curl -o $TMP_DIR/fxserver.zip $(get_server_download_url $FX_SERVER_VERSION)
  unzip -q -o $TMP_DIR/fxserver.zip -d ./.server && rm $TMP_DIR/fxserver.zip
  mkdir -p ./.server/resources
fi

# ------------------------------------------------------------------------------

if [ ! -d "./.server/resources/vMenu" ]; then
  echo ""
  echo "Downloading vMenu..."

  curl -L -o $TMP_DIR/vMenu.zip $(get_github_release_download_url "tomgrobbe/vmenu" $VMENU_VERSION)
  unzip -q -o $TMP_DIR/vMenu.zip -d $TMP_DIR/vMenu && rm $TMP_DIR/vMenu.zip

  rm $TMP_DIR/vMenu/config/permissions.cfg
  mv $TMP_DIR/vMenu ./.server/resources
fi

# ------------------------------------------------------------------------------

echo ""
echo "Symlinking server dependencies..."

ln_dir_safe "./.server/config"                      "../config"                                         && echo "Symlink created for config"
ln_dir_safe "./.server/resources/[sw-server-data]"  "../../resources"                                   && echo "Symlink created for resources"
ln_dir_safe "./.server/resources/[cfx-server-data]" "../../vendor/@citizenfx/cfx-server-data/resources" && echo "Symlink created for @citizenfx/cfx-server-data"
ln_dir_safe "./.server/resources/[shockwave]"       "../../vendor/@shockwave-cfx/shockwave"             && echo "Symlink created for @shockwave-cfx/shockwave"
ln_dir_safe "./.server/resources/[mapeditor]"       "../../vendor/@shockwave-cfx/mapeditor"             && echo "Symlink created for @shockwave-cfx/mapeditor"
ln_dir_safe "./.server/resources/[fallout]"         "../../vendor/@shockwave-cfx/fallout"               && echo "Symlink created for @shockwave-cfx/fallout"
ln_dir_safe "./.server/resources/drawline"          "../../vendor/@shockwave-cfx/drawline"              && echo "Symlink created for @shockwave-cfx/drawline"
ln_dir_safe "./.server/resources/glue"              "../../vendor/@shockwave-cfx/glue"                  && echo "Symlink created for @shockwave-cfx/glue"
ln_dir_safe "./.server/resources/gravitygun"        "../../vendor/@shockwave-cfx/gravitygun"            && echo "Symlink created for @shockwave-cfx/gravitygun"
ln_dir_safe "./.server/resources/neon"              "../../vendor/@shockwave-cfx/neon"                  && echo "Symlink created for @shockwave-cfx/neon"
ln_dir_safe "./.server/resources/nitro"             "../../vendor/@shockwave-cfx/nitro"                 && echo "Symlink created for @shockwave-cfx/nitro"
ln_dir_safe "./.server/resources/nopeds"            "../../vendor/@shockwave-cfx/nopeds"                && echo "Symlink created for @shockwave-cfx/nopeds"
ln_dir_safe "./.server/resources/notraffic"         "../../vendor/@shockwave-cfx/notraffic"             && echo "Symlink created for @shockwave-cfx/notraffic"
ln_dir_safe "./.server/resources/outrun"            "../../vendor/@shockwave-cfx/outrun"                && echo "Symlink created for @shockwave-cfx/outrun"
ln_dir_safe "./.server/resources/ramp"              "../../vendor/@shockwave-cfx/ramp"                  && echo "Symlink created for @shockwave-cfx/ramp"
ln_dir_safe "./.server/resources/rocketboost"       "../../vendor/@shockwave-cfx/rocketboost"           && echo "Symlink created for @shockwave-cfx/rocketboost"
ln_dir_safe "./.server/resources/wheelfire"         "../../vendor/@shockwave-cfx/wheelfire"             && echo "Symlink created for @shockwave-cfx/wheelfire"


# ------------------------------------------------------------------------------

echo ""
echo "Building resources..."

build_resource ./vendor/@shockwave-cfx/drawline                       && echo "Built resource drawline"
build_resource ./vendor/@shockwave-cfx/mapeditor/editor-gui           && echo "Built resource editor-gui"
build_resource ./vendor/@shockwave-cfx/shockwave/shockwave-cfx-client && echo "Built resource shockwave-cfx-client"
build_resource ./vendor/@shockwave-cfx/shockwave/shockwave-loadscreen && echo "Built resource shockwave-loadscreen"

# ------------------------------------------------------------------------------

if [ ! -f "./run.sh" ]; then
  echo "#!/bin/sh" >> ./run.sh
  echo "(cd ./.server && ./FXServer.exe +exec ../config/_server.cfg +set svgui_disable true)" >> ./run.sh
fi

# ------------------------------------------------------------------------------

echo ""
echo "Cleaning up..."

rm -r $TMP_DIR

# ------------------------------------------------------------------------------

echo ""
echo "Done! Run ./run.sh to start the server!"
echo ""
