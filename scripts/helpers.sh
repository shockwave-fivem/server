#!/bin/sh

get_latest_github_version() {
  curl -s "https://api.github.com/repos/$1/releases/latest" | \
  jq -r '.tag_name'
}

get_latest_server_version() {
  curl -s 'https://changelogs-live.fivem.net/api/changelog/versions/win32/server' | \
  jq -r '.latest_download' | \
  awk 'BEGIN { FS = "/" } ; {print $8}'
}

get_github_release_download_url() {
  curl -s "https://api.github.com/repos/$1/releases/tags/$2" | \
  jq -r '.assets[0].browser_download_url'
}

get_github_repo_download_url() {
  echo "https://github.com/$1/archive/$2.zip"
}

get_server_download_url() {
 echo "https://runtime.fivem.net/artifacts/fivem/build_server_windows/master/$1/server.zip"
}

read_input() {
  echo -e "\n$1: (Default: $2)" > /dev/tty
  read -p "> " INPUT
  echo ${INPUT:-$2}
}

ln_dir_safe() {
  local CMD="mklink /d \"${1//\//\\}\" \"${2//\//\\}\""
  [[ ! -d "$1" ]] && cmd <<< "$CMD" > /dev/null
}

build_resource() {
  (cd $1 && npm install --silent && npm run --silent build) > /dev/null
}